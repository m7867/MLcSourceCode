//import { createStore, applyMiddleware, compose } from 'redux' //compose never used? will kill Redux Dev Tools 
import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import rootReducer from './reducers'

const loggerMiddleware = createLogger()
const middleware = []

	//For Redux Dev Tools
//No Redux Dev Tools for me! //const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENTION_COMPOSE__ || compose
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__  


export default function configureStore(preloadedState){
	return createStore(
	rootReducer,
	preloadedState,
	composeEnhancers(applyMiddleware(...middleware, loggerMiddleware))
  )
}