require('babel-register');
require('babel-polyfill');
require('dotenv').config();

const HDWalletProvider = require('truffle-hdwallet-provider-privkey');
const privateKeys = process.env.PRIVATE_KEYS || ""

module.exports = {
  networks: {
    development: {
      host: "172.23.112.1", //SWEET-THROAT
      port: 7545,
      network_id: "*" // Match any network id  
    },
    kovan: {
      provider: function(){ 
        return new HDWalletProvider(
          //hide this code below
           privateKeys.split(','), // Array of account Private Keys from Ganache
          'https://kovan.infura.io/v3/${process.env.INFURA_API_KEY}'//Url to an Ethereum Node
          //hide this code abovea
        )
      },
      gas: 5000000,
      gasPrice: 25000000000,
      network_id: 42 // kovan network id  
    }
  },
  contracts_directory: './src/contracts/',
  contracts_build_directory: './src/abis/',
  compilers: {
    solc: {
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  }
}